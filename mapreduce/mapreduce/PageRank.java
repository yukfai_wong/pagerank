package mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PageRank extends Configured implements Tool {
	static class data_export_Mapper extends org.apache.hadoop.mapreduce.Mapper<LongWritable,Text, Text, Text> {
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString();
			Pattern source_target = Pattern.compile("REVISION [0-9]* [0-9]* (\\S*) ([\\S\\s]{20})|\\nMAIN ([\\S*| \\S*]*)");
			Matcher matcher = source_target.matcher(line);
			String source =null;
			String time = "";
			while (matcher.find()) {
				if(matcher.group(1)!=null) {
					if(source==null) {
						//found new source
						source = matcher.group(1);
						time = matcher.group(2)!=null?matcher.group(2):utils.ISO8601.timestampToString(context.getConfiguration().getLong("time", 0));
					}else{
						//found source do not have target
						context.write(new Text(source), new Text(time));
						source = matcher.group(1);
						time = matcher.group(2)!=null?matcher.group(2):utils.ISO8601.timestampToString(context.getConfiguration().getLong("time", 0));
					}
				}
				if(matcher.group(3)!=null) {
					//found target list
					context.write(new Text(source), new Text(time+matcher.group(3)));
					time = "";
					source = null;
				}
			}
			if(source!=null){
				//found source do not have target
				//To sure there is no missing due to data separate
				context.write(new Text(source), new Text(time));
			}
		}
	}


	 public static class socre_dst_generate_Reducer extends Reducer<Text, Text, Text, Text> {
		 public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			 Configuration conf = context.getConfiguration();
			 long limit = conf.getLong("time", 0);
			 
			 long Max_microSecond = (long) 0;
			 long values_time = (long)0;
			 String target = "";
			 
			 //find the max(time) but under limit
			 for (Text value: values) {
				 try {
					 values_time = utils.ISO8601.toTimeMS(value.toString().substring(0, 20));
					 if(values_time>Max_microSecond && values_time <= limit) {
						 Max_microSecond=values_time;
						 target = value.toString().substring(20);
					 }
				 } catch (ParseException e) {System.out.println(e);}
			 }
			 
			 
			 if(Max_microSecond!=0) {
				 if(target.equals("")) {
					 context.write(key, new Text("@"));//the ensure the page that have no outer/inner link exist
				 }else {
					 java.util.Set<String> target_unique = new java.util.HashSet<>();
					 StringTokenizer tokenizer = new StringTokenizer(target);
					 while (tokenizer.hasMoreTokens()) 
						 target_unique.add(tokenizer.nextToken());//to make all outer-link unique
					 for(String each : target_unique) {
						 if(!each.equals(key.toString())) {//no Self-loop
							 //value start with @ = dst
							 //value start with & = score
							 context.write(key, new Text("@"+each));
							 context.write(new Text(each), new Text("&1.0"));//score start at one
						 }
					 }
				 }
			 }
		 }
	 }
	 
	static class do_nothing_mapper extends org.apache.hadoop.mapreduce.Mapper<LongWritable, Text, Text, Text> {
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			StringTokenizer tokenizer = new StringTokenizer(value.toString());
			while (tokenizer.hasMoreTokens()) {
				context.write(new Text(tokenizer.nextToken()), new Text(tokenizer.nextToken()));
			}
		}
	}
	
	static class PageRanking_Reducing extends Reducer<Text, Text, Text, Text> {
		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
			Configuration conf = context.getConfiguration();
			boolean finalloop = conf.getBoolean("Final", false);
			
			ArrayList<String> out_links_list = new ArrayList<String>();
		    double PR = 0;
		    for(Text value_t : values) {
		        String value = value_t.toString();
		        if(value.substring(0, 1).equals("&")){
		        	//value after "&" is the contribute scores from other
		            PR += Double.parseDouble(value.substring(1));
		        }
		        if(value.substring(0, 1).equals("@")) {
		        	//value after "@" is the target need to contribute score
		        	out_links_list.add(value.substring(1));
		        }
		    }
		    int num_outerlink = out_links_list.size() == 0? 1: out_links_list.size();//treat page that do not have outer-link
		    PR = PR * 0.85d + 0.15d;
		    if(finalloop) {//in the last iteration, output k=source, target=score
		    	context.write(key, new Text(Double.toString(PR)));
		    }
		    else{
	    		if(out_links_list.size() == 1 && out_links_list.get(0).equals(""))//for page that have no outer link
	    			context.write(key, new Text("@"));//which allow the page that has no one link to have 0.15
	    		else {//else the list contain outer link
			    	for(String outer_link : out_links_list) {
			    		context.write(new Text(outer_link), new Text("&"+Double.toString(PR/num_outerlink)));
			    		if(!outer_link.equals(""))//avoid adding extra key ""
			    			context.write(key, new Text("@"+outer_link));
			    	}
	    		}
		    }
		}
	}
	
	 
	 public int run(String[] args) throws Exception {
		int numLoops = Integer.parseInt(args[2]);
		 //--------------------Initial process---------------------//
		Configuration conf = new Configuration();
		conf.setLong("time", utils.ISO8601.toTimeMS(args[3]));
		Job job = Job.getInstance(conf, "inital Process K=src, value = @score / &dst");
		
		
		job.setJarByClass(PageRank.class);

		job.setMapperClass(data_export_Mapper.class);
		job.setReducerClass(socre_dst_generate_Reducer.class);

		job.setInputFormatClass(MyInputFormat.class);
		FileInputFormat.setInputPaths(job, new Path(args[0]));

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		FileOutputFormat.setOutputPath(job, new Path(args[1]+"/innerProcess0"));
		
		if(!job.waitForCompletion(false))
			return 1;
		 
		//------------------Score calculating------------------------//
		
		List<Job> Joblist = new ArrayList<>();
		Joblist.add(job);
		
		boolean success = false;
		for (int i = 1; i < numLoops+1; i++) {
			conf = new Configuration();
			if(i==numLoops) {
				conf.setBoolean("Final", true);
				Joblist.add(Job.getInstance(conf, "PageRanking Loop: Final"));
				FileOutputFormat.setOutputPath(Joblist.get(i), new Path(args[1]+"/results"));
			}
			else {
				Joblist.add(Job.getInstance(conf, "PageRanking Loop:" + Integer.toString(i)));
				FileOutputFormat.setOutputPath(Joblist.get(i), new Path(args[1]+"/innerProcess"+Integer.toString(i)));
			}
			
			Joblist.get(i).setJarByClass(PageRank.class);

			Joblist.get(i).setMapperClass(do_nothing_mapper.class);
			Joblist.get(i).setReducerClass(PageRanking_Reducing.class);
			
			Joblist.get(i).setInputFormatClass(MyInputFormat.class);
			FileInputFormat.setInputPaths(Joblist.get(i), new Path(args[1]+"/innerProcess"+Integer.toString(i-1)+"/*"));

			Joblist.get(i).setOutputKeyClass(Text.class);
			Joblist.get(i).setOutputValueClass(Text.class);
			Joblist.get(i).setOutputFormatClass(TextOutputFormat.class);
		    
		    success = Joblist.get(i).waitForCompletion(false);
		    
		    
		    //delete the previous innerProcess directory
			FileSystem hdfs = FileSystem.get(conf);
		    if (hdfs.exists(new Path(args[1]+"/innerProcess"+Integer.toString(i-1))))
		      hdfs.delete(new Path(args[1]+"/innerProcess"+Integer.toString(i-1)), true);
		    
			if (!success) {
				break;
			}
		}
		//---------------------Final -------------------//
		
		FileSystem hdfs = FileSystem.get(new Configuration());
		if (hdfs.exists(new Path(args[1]+"/innerProcess"+Integer.toString(numLoops))))
		      hdfs.delete(new Path(args[1]+"/innerProcess"+Integer.toString(numLoops)), true);
		
		if(hdfs.exists(new Path(args[1]+"/results"))) {
			//move the result from results directory to real output path
			FileStatus[] directory_content = hdfs.listStatus(new Path(args[1]+"/results"));
			for(FileStatus content: directory_content) {
				if(content.isFile())
					org.apache.hadoop.fs.FileUtil.copy(hdfs, content.getPath(), hdfs, new Path(args[1]+'/'+content.getPath().getName()), true, new Configuration());
			}
		}
		if (hdfs.exists(new Path(args[1]+"/results")))
		      hdfs.delete(new Path(args[1]+"/results"), true);
		return (success ? 0 : 1);
	}
	public static void main(String[] args) throws Exception {
		 System.exit(ToolRunner.run(new Configuration(), new PageRank(), args));
	}
}