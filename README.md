Our Spark PageRank job can separate into three parts: 
1) Initial process

The code for initial process is similar with previous assessment as we ca. The previous explanation is provided below

The initial process is a MapReduce job. It read user indicate input file path and use a self-define InputFormatClass call MyInputFormat to separate the text file to Mapper. The output store at user_indicate_output_path/innerProcess0 

MyInputFormat ensures the input file split by “/n/n” because each record is ending with two next line symbols (one for TEXTDATA and one for empty line). This procedure makes sure each Mapper will only read a complete revision.
Mapper:
The Mapper use regex expression to extract article_title, timestamp and outer-link(MAIN) from each record. The output of Mapper are both Text() and in the following format: 
key: article_title    value: timestamp+outer-link
The format of the value is String+String. It can be simply separated because timestamp is in ISO8601 format which always with length 20.
Reducer: 
Reducer read the output from the mapper, filter revision with maximum data under user indication date. It also adjusts the data to the format that our page rank algorithm can read.
The reducer automatically groups all the input by its key. So that we can iterate the values under each key to find the revision with maximum data that under user indication date (values_time>Max_microSecond && values_time <= limit). 
We then store the out-link by separate the value at index 20. This group of outer-links is a String with space to separate them. We use StringTokenizer to split out-link and store in a Set if it is different from the key to ensure it is a simple graph.
The Reducer output the set of outer-links in two formats:
#1: key: key(article name)    value: “@”+outer-link
This kind of k-v pair ensure each round of page rank the source article know which target articles it needs to contribute its page rank score
#2: key: outer-link        value: “&1.0”
This kind of k-v pair contributes the page rank score to each outer-link. By the documentation, the score initial at 1.0.


2)Page Ranking
The page ranking algorithm is a looping MapReduce job read the output of Initial process and Page Ranking itself. Each looping will store the output at user_indicate_output_path/innerProcessX where X is the number of rounds.  At the end of each loop, the input will be deleted (innerProcessX). However, the final round will store the output at user_indicate_output_path/results
Mapper:
It is a mapper that do nothing but read each line and put it in key-value pair
Reducer:
The Reducer will receive and separate the k-v pair by the starting of value (“@” or ”&”).
So for each page: 
The reducer can collect the target that this(this key) page need to contribute. (k-v pair#1)
The reducer can collect the page rank score from the contribution of each source page. (k-v pair#2)

The target will store as a list and the page rank scores used to calculate the new score for this page.
The Reducer will have two types of output:
If not last round:
    #1: same format as input
If last round:
#2: key: key(article name)    value: page rank score
When creating the job of last round, the program uses conf.setBoolean("Final", true); to tell all reducer it is last round.


3)final move
After all rank complete the result stored at user_indicate_output_path/results. We use FileSystem.listStatus to list all the file in the results directory. Then we copy all files to user_indicate_output_path/ and delete the file while coping by using FileUtil.copy. After coping complete results directory will be removed.


(b) any interesting aspects of your solution(e.g., assumptions you’ve made, optimisations that you thought of, etc.)

We have tried to improve our Initial process by separate input file by the number of lines. The number of lines is the multiple of 14(to ensure no REVISION being split) and depends on how many mappers we need. For example, separate by 28 will give us two REVISIONs per Mapper and create thousands of Mapper.
We have successfully implemented by further modify MyInputFormat.java. However, we have commented out and not going to use it because it required to download the input file to local for separation and not efficient if the input file is large.



(a) your solution (key-value format for mappers and reducers, as well as any other configuration details for your jobs)


######################################################
Our Spark PageRank job can separate into three parts: 
1) Initial process

The code for the initial process is similar to previous assessment as the previous code can be input by using newAPIHadoopFile. The documentation has shown as following.
We have implemented a new input MyInputFormat class to ensure each revision is read completely. Since each record is ending with two next line symbols (one for TEXTDATA and one for empty line), the input file will split by “\n\n”.

The initial process is a Map type task. We have used flatMapToPair to generate source value pair. Each block of data is applied a regular expression to extract source, view data and target from each REVISION. The result will be grouped by using groupByKey() function with a key. This procedure is ending up with a collection that contains all the REVISION under the key. A filter is applied to select the REVISION with maximum data but under user indication date (values_time>Max_microSecond && values_time <= limit). The outer-link are being separated to  generate key-value pair in the following format:
key: article name(source)    value: outer-link(target)
The pair will be grouped by key to ensure it is intractable. The collections are cache into memory by using a cache() because this dataset will be used in PageRank main loop frequently.

Each source article was applied a default score of 1.0 by mapValues. 

We then right outer join the link to the score for ensuring each score know where it needs to contribute. The reason for using right outer join is that we need to make sure the link that does not have a target existing in the dataset. However, if the target does not exist it will give itself a zero score to ensure it is existing. Then, the score will be grouped by the key which is the process of each page collecting the score and apply the PageRank algorithm. The result can be used to apply another right outer join to form the round of PageRank.

After the calculation, we apply a simple map to transform JavaPairRDD to a simple JavaRDD because JavaRDD contains a build-in sortBy function which is far easier to implement the result sorting requirement. Once the result is sorted, it will store at use indicate path.