package spark;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

import com.amazonaws.thirdparty.apache.http.ParseException;
import com.google.common.collect.Iterables;

import scala.Tuple2;

public class pagerank {

	public static void main(String[] args) throws Exception {
		JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("Page Rank"));
		
		final long limit = utils.ISO8601.toTimeMS(args[3]);
		
		JavaPairRDD<String, Iterable<String>> links = sc.newAPIHadoopFile(args[0], mapreduce.MyInputFormat.class, LongWritable.class, Text.class,new org.apache.hadoop.conf.Configuration())
		.flatMapToPair(each_record -> {
			List<Tuple2<String, String>> res = new ArrayList<Tuple2<String, String>>();
			
			Pattern source_target = Pattern.compile("REVISION [0-9]* [0-9]* (\\S*) ([\\S\\s]{20})|\\nMAIN ([\\S*| \\S*]*)");
			Matcher matcher = source_target.matcher(each_record.toString());
			String source =null;
			String time = "";
			while (matcher.find()) {
				if(matcher.group(1)!=null) {
					if(source==null) {
						//found new source
						source = matcher.group(1);
						time = matcher.group(2);
					}else{
						//found source do not have target
						res.add(new Tuple2<String, String>(source, time));
						source = matcher.group(1);
						time = matcher.group(2);
					}
				}
				if(matcher.group(3)!=null) {
					//found target list
					res.add(new Tuple2<String, String>(source, time+matcher.group(3)));
					time = "";
					source = null;
				}
			}
			if(source!=null){
				//found source do not have target
				//To sure there is no missing due to data separate
				res.add(new Tuple2<String, String>(source, time));
			}
			return res; 
		})
//		.filter(t -> utils.ISO8601.toTimeMS(t._2().substring(0, 20)) <= limit)
		.groupByKey()
		.flatMapToPair(pair -> {
			List<Tuple2<String, String>> res = new ArrayList<Tuple2<String, String>>();
			
			long Max_microSecond = Long.MIN_VALUE;
			long value_time;
			String target = "";
			for(String value:pair._2()) {
				 try {
					 value_time = utils.ISO8601.toTimeMS(value.toString().substring(0, 20));
					 if(value_time>Max_microSecond && value_time <= limit) {
						 Max_microSecond=value_time;
						 target = value.substring(20);
					 }
				 } catch (ParseException e) {System.out.println(e);}
			}
			if(Max_microSecond!=Long.MIN_VALUE) {
				if(target.equals("")) {
					res.add(new Tuple2<String, String>(pair._1(), ""));//the ensure the page that have no outer/inner link exist
				}else {
					java.util.Set<String> target_unique = new java.util.HashSet<>();
					StringTokenizer tokenizer = new StringTokenizer(target);
					while (tokenizer.hasMoreTokens()) 
						target_unique.add(tokenizer.nextToken());//to make all outer-link unique
					for(String each : target_unique) {
						if(!each.equals(pair._1())) {//no Self-loop
							//value start with @ = dst
							//value start with & = score
							res.add(new Tuple2<String, String>(pair._1(), each));
						}
					}
				}
			}
			return res;
		})
		.groupByKey()
		.cache();
		
		int numIterations = Integer.parseInt(args[2]);
		JavaPairRDD<String, Double> ranks = links.mapValues(s -> 1.0);
//		JavaPairRDD<String, Tuple2<Optional<Iterable<String>>, Double>> contribs = links.rightOuterJoin(ranks);
		for (int current = 0; current < numIterations; current++) {
			JavaPairRDD<String, Double> contribs = links.rightOuterJoin(ranks)
				.flatMapToPair(v -> {
					List<Tuple2<String, Double>> res = new ArrayList<Tuple2<String, Double>>();
					if(v._2()._1().isPresent()) {
						int urlCount = Iterables.size(v._2()._1().get());
						if(urlCount>0) {
							for (String s : v._2()._1().get())
								if(!s.equals(""))
									res.add(new Tuple2<String, Double>(s, (v._2()._2())/(urlCount==0?1:urlCount)));
						}else
							res.add(new Tuple2<String, Double>(v._1, 0D));//no outer link
					}
					res.add(new Tuple2<String, Double>(v._1, 0D));//leaf node
					return res;
				});
			ranks = contribs.reduceByKey((a, b) -> a + b).mapValues(v -> 0.15 + v * 0.85);
		}
		ranks.map(t -> t).sortBy(pair -> pair._2, false, 1).saveAsTextFile(args[1]);
		//sortBy only support by JavaRDD, we use map to cover JavaPairRDD to JavaRDD
		sc.close();
	}
}
